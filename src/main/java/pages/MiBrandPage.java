package pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class MiBrandPage extends ProjectMethods {
		public MiBrandPage() {
			PageFactory.initElements(driver, this);
		}
		@FindBy(xpath="//div[text()='Newest First']") WebElement eleNew;
		public MiBrandPage clickNewestFirst() throws InterruptedException {
			click(eleNew);	
			Thread.sleep(2000);
			return this;
		}
		
		@FindBy(xpath="//div[@class=\"_3wU53n\"]") List<WebElement> elePrintProductName;
		@FindBy(xpath="//div[@class=\"_1vC4OE _2rQ-NK\"]") List<WebElement> elePrintProductPrice;
		public RedMiPage printProductNameAndPrice() {
			for (int i = 0; i < elePrintProductName.size(); i++) {
				System.out.println(elePrintProductName.get(i).getText()+"  "
			+elePrintProductPrice.get(i).getText().replaceAll("\\D", ""));
			}
	
			return new RedMiPage();
		}
		
		
		
			
		}
		/*public MiBrandPage printProductPrice() {
			//getText(elePrintProductPrice);
			
			
			return this;
		}
		*/
		

